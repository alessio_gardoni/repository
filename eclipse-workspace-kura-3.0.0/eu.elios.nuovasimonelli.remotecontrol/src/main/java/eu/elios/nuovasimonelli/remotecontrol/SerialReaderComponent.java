package eu.elios.nuovasimonelli.remotecontrol;

import java.util.Map;
import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import org.eclipse.kura.KuraException;
import org.eclipse.kura.cloud.Cloudlet;
import org.eclipse.kura.configuration.ConfigurableComponent;
import org.eclipse.kura.message.KuraPayload;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SerialReaderComponent extends Cloudlet implements ConfigurableComponent
{
	private static final Logger Logger = LoggerFactory.getLogger(SerialReaderComponent.class);
	private static final String APP_ID = "eu.elios.nuovasimonelli.remotecontrol.SerialReaderComponent";
	private static final ScheduledExecutorService Executor = Executors.newSingleThreadScheduledExecutor();
	private static final Random Randomizer = new Random();
	
	private Map<String, Object> properties;
	private ScheduledFuture<?> handle;

	public SerialReaderComponent()
	{
		super(APP_ID);
	}

	protected void activate(ComponentContext componentContext, Map<String, Object> properties)
	{
		super.activate(componentContext);
		
		this.properties = properties;
		Logger.info("Il componente " + APP_ID + " si � attivato.");

		handle = Executor.scheduleWithFixedDelay(new Runnable()
		{
			@Override
			public void run()
			{
				// while (true)
				{
					onTimerTick();

					/*
					 * try { Thread.sleep(100); } catch (InterruptedException e) { break; }
					 */
				}
			}
		}, 2000, 60000, TimeUnit.MILLISECONDS);
	}

	protected void modified(Map<String, Object> properties)
	{
		this.properties = properties;
		Logger.info("La configurazione del componente " + APP_ID + " � stata modificata.");
	}

	protected void deactivate(ComponentContext componentContext)
	{
		super.deactivate(componentContext);
		Logger.info("Il componente " + APP_ID + " si è disattivato.");
	}

	private void onTimerTick()
	{
		KuraPayload payload = new KuraPayload();
		payload.addMetric("random", Randomizer.nextInt());
		try
		{
			getCloudApplicationClient().publish("test/publish", payload, 0, false);
		} catch (KuraException e)
		{
			Logger.error(e.getMessage());
		}

		//Logger.info("Lettura dalla seriale...");
	}
}
